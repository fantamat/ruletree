import numpy as np
import tensorflow as tf


def tf_sigmoid_inversion(x):
    return tf.log(x/(1-x))

def np_sigmoid_inversion(x):
    return np.log(x/(x-1))


def fill_default_params(default_params, params):
    for default_param in default_params:
        if default_param not in params:
            params[default_param] = default_params[default_param]