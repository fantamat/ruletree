from ruleex.deepred.core import *
from ruleex.deepred.core import __merge
from ruleex.deepred.rule import PositionAxisRule


def deepred_cnn(layers_activations, architecture, params):
    """

    :param layers_activations: list of numpy arrays,
        first is the input layer and it has tensor shape with spacial dimension
        and the output layer (Y) is the last
    :param architecture: list of a tupples that means:
        ("Input",): Input layer that has to be first, shape is the shape of the input first indexies are spacial and the last is chanel
        ("FC",): fully conected layer
        ("CNN", $filter_size$): all attributes are list with two integers ($stride$ = (1,1), $padding$ hold same size as input, i.e., filter_size/2
        ("Pool", $type$, $stride$): $filter_size$ is cosidered same as stride
        ("Output",): Output layer after softmax normalization
    :param dtc_params: dictionary,
    parameters passed to the decition tree algortihm sklearn.tree.DecisionTreeClassifier(dtc_params)
    :return: ruletree as extracted ddag
        in dictionary params under key "inf" following informations:
            inf["dag_size"]: list of the dag size while training
            inf["size_befor_reduc"]: list of ddag size before reduction
            inf["fidelity"]: fidelity for each step
            inf["sub_tree_size"]: list of all sizes of subtituing trees
            inf["sub_tree_acc"]: their accuracies
        if SAVE_PROCESS is enebled then also contain
            inf["dag"]: dags for each step of alg
            inf["sub_tree"]: dictionary of subtituing trees for each step
    """

    # function definitions
    def add(tup1, tup2):
        return (tup1[0]+tup2[0], tup1[1]+tup2[1])

    def mult(tup1, tup2):
        return (tup1[0] * tup2[0], tup1[1] * tup2[1])

    def get_layer_info(tup):
        """
        tup:
        ("CNN", $filter_size$, $stride$, $padding$)
        ("Pool", $type$, $filter_size$, $stride$, $padding$)"""
        out = dict()
        if tup[0] == "CNN":
            out["isCNN"] = True
            out["isPool"] = False
            out["fsize"] = tup[1]
        elif tup[0] == "Pool":
            out["isConv"] = True
            out["isPool"] = False
            if tup[1] == "max":
                out["isMax"] = True
            else:
                out["isMax"] = False
            if tup[1] == "mean":
                out["isMean"] = True
            else:
                out["isMean"] = False
            out["stride"] = tup[2]
        else:
            raise TypeError("Wrong structure of the architecture parameter, please read documentation.")

    def make_index_map_2D(size, layer_info):
        if layer_info["isConv"]:
            s = (size[0], size[1])
        else:
            s = (size[0]//layer_info["stride"][0], size[1]//layer_info["stride"][1])
        prev_index = np.reshape(np.arange(s[0]*s[1]), s)
        out = dict()
        for i in range(s[0]):
            for j in range(s[1]):
                out[prev_index[i,j]] = (i,j)
        return out

    def make_index_map_3D(size, fsize):
        s = (fsize[0], fsize[1], size[2])
        from_spacial = np.reshape(np.arange(s[0]*s[1]*s[2]),s)
        to_spacial = dict()
        for i in range(s[0]):
            for j in range(s[1]):
                for k in range(s[2]):
                    to_spacial[from_spacial[i,j,k]] = (i,j,k)
        return from_spacial, to_spacial

    def convert_2_position(dag: RuleTree, index_map=None, r=(1,1), pos=(0,0), dis=(0,0), to_spacial=None, fsize=None):
        def convert_2_position_rek(node):
            if isinstance(node, AxisRule):
                if index_map:
                    new_node = PositionAxisRule(index_map[node.i], r, dis, 1, node.b)
                else:
                    s0,s1,s2 = to_spacial(node.i)
                    new_node = PositionAxisRule(pos, r,
                                                add(dis, (s0-fsize[0]//2,s1-fsize[1]//2)),
                                                s2, node.b)
                new_node.true_branch = convert_2_position_rek(node.true_branch)
                new_node.false_branch = convert_2_position_rek(node.false_branch)
                return new_node
        dag.root = convert_2_position_rek(dag.root)
        return dag

    def get_spacial_threshlods(all_nodes):
        out = set()
        for node in all_nodes:
            if isinstance(node, PositionAxisRule):
                out.add((node.i, node.b))
        return out

    def max_pool_transform(all_nodes, r):
        for node in all_nodes:
            if isinstance(node, PositionAxisRule):
                node.range = mult(node.range, r)
                node.pos = mult(node.pos, r)
                node.dis = mult(node.dis, r)

    def getXforLayer(actLyer):
        return layers_activations[actLyer]

    def getY(actLayer, i, th):
        return (layers_activations[actLayer + 1][:, i] > th)

    def getSpatialY(actLayer, i, th):
        x = layers_activations[actLayer + 1]
        s0, s1, s2, _ = x.shape
        out = x[:, :, :, i] > th
        return out.reshape(s0, s1*s2)

    def getXforSpatialLayer(actLayer, fsize):
        x = layers_activations[actLayer]
        if len(x.shape) > 3:
            x = np.pad(x, [(0,0),
                           (fsize[0] // 2, fsize - 1 - fsize[0] // 2),
                           (fsize[1] // 2, fsize - 1 - fsize[1] // 2),
                           (0, 0)],
                       mode='constant')
            out = list()
            for i in range(x.shape[1]-fsize[0]+1):
                for j in range(x.shape[2]-fsize[1]+1):
                    out.append(x[:,i:i+fsize[0],j:j+fsize[1],:])
            out = np.concatenate(tuple(out))
            return out.reshape(out.shape[0], fsize[0]*fsize[1]*out.shape[2])
        else:
            x = np.pad(x, [(0,0),
                           (fsize[0] // 2, fsize - 1 - fsize[0] // 2),
                           (fsize[1] // 2, fsize - 1 - fsize[1] // 2)],
                       mode='constant')
            out = list()
            for i in range(x.shape[1]-fsize[0]+1):
                for j in range(x.shape[2]-fsize[1]+1):
                    out.append(x[:,i:i+fsize[0],j:j+fsize[1]])
            out = np.concatenate(tuple(out))
            return out.reshape(out.shape[0], fsize[0]*fsize[1])

    def remove_out_of_bounds(rt, all_nodes):
        prevs = rt.get_predecessor_dict(all_nodes)
        for node in all_nodes:
            if isinstance(node, PositionAxisRule):
                p = add(node.pos, node.dis)
                if p[0]<0 or p[1]<0: #todo or p[0]>...
                    if node.b > 0:
                        rt.delete_node(node, True, all_nodes, prevs)
                    else:
                        rt.delete_node(node, False, all_nodes, prevs)
        return rt


    def __merge_spacial(rt, dt, to_spacial, fsize, all_nodes=None):
        rt.type = "deepred-ddag"

        def rulesToSubs(all_rules):
            output = list()
            for rule in all_rules:
                if type(rule) is PositionAxisRule:
                    output.append(rule)
            return output

        def mergeDT(root):
            actrt = dt[(root.i, root.b)].copy()
            actrt = convert_2_position(actrt, r=root.range, pos=root.pos, to_spacial=to_spacial, fsize=fsize)
            all_rules = actrt.get_all_nodes()
            actrt = remove_out_of_bounds(actrt, all_nodes)
            actrt = actrt.replace_leaf_with_set([1], root.true_branch, all_nodes=all_rules)
            actrt = actrt.replace_leaf_with_set([0], root.false_branch, all_nodes=all_rules)
            if root.true_branch:
                all_rules.add(root.true_branch)
            if root.false_branch:
                all_rules.add(root.false_branch)
            new_pred = actrt.get_predecessor_dict(all_nodes=all_rules)
            rt.replace_rule(root, actrt.root, pred_entries=pred[root])
            predEntries = pred[root]
            del pred[root]
            pred[actrt.root] = predEntries
            if root.true_branch in new_pred:
                pred[root.true_branch] = new_pred[root.true_branch]
            if root.false_branch in new_pred:
                pred[root.false_branch] = new_pred[root.false_branch]

        if not all_nodes:
            all_nodes = rt.get_all_nodes()
        pred = rt.get_predecessor_dict(all_nodes)
        # merge
        rts = rulesToSubs(all_nodes)
        for i, rule in enumerate(rts):
            if (rule.i, rule.b) in dt:
                mergeDT(rule)
        return rt

    # the algorithm

    fill_default_params(params)
    lastLayerIndex = len(layers_activations) - 1
    dt_base = tree.DecisionTreeClassifier(**params[INITIAL_DT_TRAIN_PARAMS])
    if params[BUILD_FIRST]:
        dt = dt_base.fit(getXforLayer(lastLayerIndex - 1), values_2_classes(getXforLayer(lastLayerIndex)))
        rt = sklearndt_to_ruletree(dt, params[ONE_CLASS_ON_LEAF],
                                min_rule_samples=params[MIN_RULE_SAMPLES],
                                min_split_fraction=params[MIN_SPLIT_FRACTION])
    else:
        rt = RuleTree.half_in_ruletree(len(layers_activations[-1][0]))
    all_nodes = rt.get_all_nodes()
    a = rt.eval_all(getXforLayer(-2))
    b = values_2_classes(getXforLayer(-1))
    fidelity = np.sum(a==b)/len(a)
    if params[VARBOSE] > 0:
        print("[deepred]: Layer {} fidelity {}".format(lastLayerIndex-1, fidelity))
    inf = dict()
    inf["dag_size"] = [len(all_nodes)]
    inf["size_befor_reduc"] = [len(all_nodes)]
    inf["fidelity"] = [fidelity]
    inf["sub_tree_size"] = list()
    inf["sub_tree_acc"] = list()
    if params[SAVE_PROCESS]:
        inf["dag"] = [rt.copy()]
        inf["sub_tree"] = list()
    spacial = False
    for actLayer in reversed(range(lastLayerIndex-1)):
        if architecture[actLayer] not in {("FC",), ("Output",)} and not spacial:
            spacial = True
            endLayer = actLayer
            break
        if params[VARBOSE] > 0:
            print("[deepred]: Working on the layer {} with type {}".format(actLayer, architecture[actLayer]))

        threshold = rt.get_thresholds(all_nodes=all_nodes)
        dt = dict()
        counter = 0
        if params[SAVE_PROCESS]:
            inf["sub_tree"].append(dict())
        inf["sub_tree_acc"].append(list())
        inf["sub_tree_size"].append(list())
        for i, th in threshold:
            counter += 1

            if params[VARBOSE] > 1:
                print("[deepred]: Processing decision tree for {} < x_{} ({}\{})".format(th, i, counter, len(threshold)))
            dt_base = tree.DecisionTreeClassifier(**params[DT_TRAIN_PARAMS])
            x = getXforLayer(actLayer)#[indexes[(i, th)]]
            y = getY(actLayer, i, th)#[indexes[(i, th)]]

            dt[(i, th)] = sklearndt_to_ruletree(dt_base.fit(x, y), params[ONE_CLASS_ON_LEAF],
                                             min_rule_samples=params[MIN_RULE_SAMPLES],
                                             min_split_fraction=params[MIN_SPLIT_FRACTION])
            if params[SAVE_PROCESS]:
                inf["sub_tree"][-1][(i,th)] = dt[(i,th)].copy()
            a = dt[(i, th)].eval_all(x)
            accuracy = np.sum(a == y) / len(a)
            inf["sub_tree_acc"][-1].append(accuracy)
            inf["sub_tree_size"][-1].append(len(dt[(i,th)].get_all_nodes()))
            if params[VARBOSE] > 1:
                print("[deepred]: -- accuracy: {}".format(accuracy))
        rt = __merge(rt, dt)
        all_nodes = rt.get_all_nodes()
        prevs = rt.get_predecessor_dict(all_nodes)
        a = rt.eval_all(getXforLayer(actLayer), all_nodes, prevs)
        b = values_2_classes(getXforLayer(-1))
        fidelity = np.sum(a == b) / len(a)
        if params[VARBOSE] > 0:
            print("[deepred]: layer {} fidelity {}".format(actLayer, fidelity))
        inf["size_befor_reduc"].append(len(all_nodes))
        inf["fidelity"].append(fidelity)
        rt = rt.remove_unused_edges(all_nodes, prevs)
        rt.input_size = len(layers_activations[actLayer][0])
        inf["dag_size"].append(len(all_nodes))
        if params[VARBOSE] > 2:
            rt.view_graph(params[OUTPUT_DIR] + "deepred_layer_{}".format(actLayer))
    if not spacial:
        params[INF] = inf
        return rt
    s = getXforLayer(endLayer)[0].shape
    linf = get_layer_info(architecture[endLayer])
    index_map = make_index_map_2D(s, linf)
    rt = convert_2_position(rt, index_map)
    all_nodes = rt.get_all_nodes()
    prevs = rt.get_predecessor_dict(all_nodes)
    dependency_range = dict()
    dependency_range[endLayer+1] = (1,1)
    prev_is_mean = 0
    for actLayer in reversed(range(endLayer+1)):
        if params[VARBOSE] > 0:
            print("[deepred]: Working on the spacial layer {} with type {}".format(actLayer, architecture[actLayer]))
        linf = get_layer_info(architecture[actLayer])
        if linf["isPool"] and linf["isMax"]:
            max_pool_transform(all_nodes, linf["stride"])
            dependency_range[actLayer] = mult(dependency_range[actLayer+1], linf["stride"])
            prev_is_mean = 0
            continue
        if linf["isPool"] and linf["isMean"]:
            dependency_range[actLayer] = mult(dependency_range[actLayer+1], linf["stride"])
            prev_is_mean = linf["stride"]
            continue
        dependency_range[actLayer] = add(dependency_range[actLayer+1], add(linf["fsize"], (-1, -1)))
        if prev_is_mean:
            linf["fsize"] = mult(linf["fsize"], prev_is_mean)
        threshold = get_spacial_threshlods(all_nodes=all_nodes)
        dt = dict()
        counter = 0
        inf["sub_tree_acc"].append(list())
        inf["sub_tree_size"].append(list())
        x = getXforSpatialLayer(actLayer, linf["fsize"])
        for i, th in threshold:
            counter += 1

            if params[VARBOSE] > 1:
                print("[deepred]: Processing decision tree for x_{} > {} range {} ({}\{})".format(i, th, range, counter, len(threshold)))
            dt_base = tree.DecisionTreeClassifier(**params[DT_TRAIN_PARAMS])
            y = getSpatialY(actLayer, i, th)

            dt[(i, th)] = sklearndt_to_ruletree(dt_base.fit(x, y), params[ONE_CLASS_ON_LEAF],
                                             min_rule_samples=params[MIN_RULE_SAMPLES],
                                             min_split_fraction=params[MIN_SPLIT_FRACTION])
            a = dt[(i, th)].eval_all(x)
            accuracy = np.sum(a == y) / len(a)
            inf["sub_tree_acc"][-1].append(accuracy)
            inf["sub_tree_size"][-1].append(len(dt[(i,th)].get_all_nodes()))
            if params[VARBOSE] > 1:
                print("[deepred]: -- accuracy: {}".format(accuracy))
        to_spacial = make_index_map_3D(getXforLayer(actLayer)[0].shape, linf["fsize"])
        rt = __merge_spacial(rt, dt, to_spacial, fsize=linf["fsize"]) #todo different mege
        all_nodes = rt.get_all_nodes()
        prevs = rt.get_predecessor_dict(all_nodes)
        a = rt.eval_all(getXforLayer(actLayer), all_nodes, prevs)
        b = values_2_classes(getXforLayer(-1))
        fidelity = np.sum(a == b) / len(a)
        if params[VARBOSE] > 0:
            print("[deepred]: layer {} fidelity {}".format(actLayer, fidelity))
        inf["size_befor_reduc"].append(len(all_nodes))
        inf["fidelity"].append(fidelity)
        rt = rt.remove_unused_edges(all_nodes, prevs)
        rt.input_size = len(layers_activations[actLayer][0])
        inf["dag_size"].append(len(all_nodes))
        if params[VARBOSE] > 2:
            rt.view_graph(params[OUTPUT_DIR] + "deepred_layer_{}".format(actLayer))

    rt.type = "Decition graph generated by DEEPRED_CNN"
    rt = rt.delete_redundancy()
    if params[VARBOSE] > 2:
        rt.view_graph(filename=params[OUTPUT_DIR] + "Result")
    params[INF] = inf
    return rt
