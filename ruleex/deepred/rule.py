from ruleex.tree.rule import AxisRule
import numpy as np


class PositionAxisRule(AxisRule):
    __slots__ = ("pos", "range", "dis", "i", "b")
    """
    representation of rule extracted from CNN attributes stand for:
        pos: position of the left upper corner of dependency range
        range: width were the condition holds at least once
        dis: displacement from the position
        i: index of the chanel
        b: threshold
    In addition, the position cold be negative if after summing with dis will be non-negative
    
    final rule has a form 
        there exists (k,l) in the range that satisfy x(pos1+dis1+ k, pos2+dis2+ l)_i > b 
    """

    def __init__(self, pos, range, dis, i, b, trueBranch=None, falseBranch=None, classSet=set()):
        super().__init__(i, b, trueBranch,falseBranch,classSet)
        self.pos = pos
        self.range = range
        self.dis = dis

    def eval_rule(self, x):
        return np.any(x[self.pos[0] + self.dis[0] : self.pos[0] + self.dis[0] + range[0],
                     self.pos[1] + self.dis[1] : self.pos[1] + self.dis[1] + range[1],
                     self.i] > self.b)

    def eval_all(self, x):
        out = np.any(np.any(x[:,
                     self.pos[0] + self.dis[0] : self.pos[0] + self.dis[0] + range[0],
                     self.pos[1] + self.dis[1] : self.pos[1] + self.dis[1] + range[1],
                     self.i] > self.b, axis=2), axis=1)
        self.num_true = np.sum(out)
        self.num_false = len(x) - self.num_true
        return out

    def to_string(self, long=False):
        ad = ""
        if long and (self.num_true or self.num_false):
            ad = "\nT:{} F:{}".format(self.num_true, self.num_false)
        else:
            ad = ""
        return "({p0},{p1}): x_({d0}+l,{d1}+k)_{i} > {b}".format(
            p0=self.pos[0], p1=self.pos[1],
            d0=self.dis[0], d1=self.dis[1],
            i=self.i, b=self.b) + ad

    def copy(self):
        output = PositionAxisRule(self.pos, self.dis, self.range, self.i,self.b, classSet=self.class_set)
        if self.true_branch:
            output.true_branch = self.true_branch.copy()
        if self.false_branch:
            output.false_branch = self.false_branch.copy()
        return output

