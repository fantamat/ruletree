from ruleex.tree.rule import AxisRule, LinearRule, Leaf
from ruleex.tree.ruletree import RuleTree
from numpy import array as ar

def runTest():
    # constants definitions
    insize = 2
    classnum = 3

    rt = RuleTree(num_class=classnum, input_size=insize)
    rt.type = "if-then"
    leaf1 = Leaf(classSet={0})
    leaf2 = Leaf(classSet={1})
    leaf3 = Leaf(classSet={2})
    rt.root = AxisRule(0, 1.0, LinearRule(ar([1,2]),1.0,leaf1,leaf2),
                       AxisRule(i=1,b=2.0,trueBranch=leaf2,falseBranch=leaf3))

    a = rt.eval_one(ar([1.1, -1]))
    b = rt.eval_all(ar([[2, 0], [0, -1]]))
    rt.fill_hits(ar([[2,0],[0,-1]]), [0,2])
    rt.fill_class_sets()
    print(a)
    print(b)
    print(rt.to_string())

    rt.view_graph(filename="testGraph.gv")
    # test empty tree
    rt = RuleTree(classnum,insize)
    a = rt.eval_one(ar([1.0, -4.5]))
    print(a)

    print(rt.to_string())

runTest()